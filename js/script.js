// ####    ####    ####    ####    ####    ####    ####
// Data and Variables
// ####    ####    ####    ####    ####    ####    ####

const quizData = {
  form: {
    action: "https://go.curry.edu/l/504481/2020-01-23/88bvk",
    method: "post",
    enctype: "application/x-www-form-urlencoded",
    id: "quiz-form"
  },
  questions: [
    {
      title: "My idea of a great weekend is spending it:",
      name: "great weekend",
      answers: [
        {
          answer: "Outdoors",
          persona: "Trail Blazer"
        },
        {
          answer: "Studying",
          persona: "Fast Tracker"
        },
        {
          answer: "Chilling",
          persona: "Relationship Builder"
        },
        {
          answer: "At the big game",
          persona: "Game Changer"
        },
        {
          answer: "Interning",
          persona: "Fast Tracker"
        },
        {
          answer: "Gaming",
          persona: "Relationship Builder"
        }
      ]
    },
    {
      title: "I like to make a difference by:",
      name: "make a difference",
      answers: [
        {
          answer: "Rallying for a cause",
          persona: "Game Changer"
        },
        {
          answer: "Volunteering at an animal shelter",
          persona: "Trail Blazer"
        },
        {
          answer: "Cleaning up my local park",
          persona: "Trail Blazer"
        },
        {
          answer: "Holding a recycling drive",
          persona: "Fast Tracker"
        },
        {
          answer: "Raising money",
          persona: "Relationship Builder"
        },
        {
          answer: "Hella tweeting",
          persona: "Game Changer"
        }
      ]
    },
    {
      title: "My dream career:",
      name: "dream career",
      answers: [
        {
          answer: "Communications & Entertainment",
          persona: "none"
        },
        {
          answer: "STEM / Health Care",
          persona: "none"
        },
        {
          answer: "Business / Entrepreneurial",
          persona: "none"
        },
        {
          answer: "Education",
          persona: "none"
        },
        {
          answer: "Criminal / Social Justice",
          persona: "none"
        },
        {
          answer: "Not Sure Yet",
          persona: "none"
        }
      ]
    },
    {
      title: 'I want my future job to be primarily:',
      name: 'future job',
      answers: [
        {
          answer: 'On the Fast Track',
          persona: 'Fast Tracker'
        },
        {
          answer: 'Flexible',
          persona: 'Relationship Builder'
        },
        {
          answer: 'Team Oriented',
          persona: 'Game Changer'
        },
        {
          answer: 'Environmentally Friendly',
          persona: 'Relationship Builder'
        }
      ]
    },
    {
      title: 'When it’s time for a study break, I want:',
      name: 'study break',
      answers: [
        {
          answer: 'Pizza',
          persona: 'none'
        },
        {
          answer: 'Ice Cream',
          persona: 'none'
        },
        {
          answer: 'Nachos',
          persona: 'none'
        },
        {
          answer: 'Popcorn',
          persona: 'none'
        }
      ]
    },
    {
      title: 'My go-to social app is:',
      name: 'social-app',
      answers: [
        {
          answer: 'TikTok',
          persona: 'none'
        },
        {
          answer: 'Instagram',
          persona: 'none'
        },
        {
          answer: 'Facebook',
          persona: 'none'
        },
        {
          answer: 'Twitter',
          persona: 'none'
        },
        {
          answer: 'SnapChat',
          persona: 'none'
        },
        {
          answer: 'Face-to-face > Facetime',
          persona: 'none'
        }
      ]
    },
    {
      title: 'The first thing I’m going to pack for college is:',
      name: 'college packing',
      answers: [
        {
          answer: 'My game system',
          persona: 'Relationship Builder'
        },
        {
          answer: 'My computer',
          persona: 'Fast Tracker'
        },
        {
          answer: 'All the comforts of home',
          persona: 'Relationship Builder'
        },
        {
          answer: 'My team gear',
          persona: 'Game Changer'
        },
        {
          answer: 'My favorite books',
          persona: 'Fast Tracker'
        },
        {
          answer: 'Anything, as long as it’s eco-friendly',
          persona: 'Trail Blazer'
        }
      ]
    }
  ],
  userFields: [
    {
      element: "input",
      type: "text",
      name: "First Name",
      required: "required"
    },
    {
      element: "input",
      type: "text",
      name: "Last Name",
      required: "required"
    },
    {
      element: "input",
      type: "email",
      name: "Email",
      required: "required"
    },
    {
      element: "select",
      name: "Role",
      options: [
        {
          value: "Prospective Student"
        },
        {
          value: "Parent"
        },
        {
          value: "Other"
        }
      ]
    },
    {
      element: "input",
      type: "hidden",
      name: "persona"
    }
  ],
  results: [
    "Trail Blazer",
    "Fast Tracker",
    "Relationship Builder",
    "Game Changer",
    "Opportunity Seeker"
  ],
  defaultResult: "Opportunity Seeker",
  descriptions: [
    "You’re ready to make a difference, for the environment and for the future. You’re as happy outside as you are hitting the books.",
    "You’re ready to start your career, with a plan all mapped out. Internships? Sign you up.",
    "You’re ready to connect with people who share your interest and sense of adventure. Whether you’re getting involved in campus activities, hanging with friends in your room, or beating your best score at Fortnite, it’s all about the lifelong connections you’re going to build.",
    "You’re ready to get in the game, on and off the field. Whether you want to lead your team to victory or cheer loudly from the stands, it’s all about focusing on a successful season.",
    "You’re ready for anything, from meeting new people to exploring new interests. College is going to be your time to shine and discover what’s next."
  ],
  gifs: [
    ' <div style="width:100%;height:0;padding-bottom:82%;position:relative;"><iframe src="https://giphy.com/embed/VGG8UY1nEl66Y" width="100%" height="100%" style="position:absolute;left: 0;"; frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/adventure-latin-flinch-VGG8UY1nEl66Y">via GIPHY</a></p> '
  ]
};

// ####    ####    ####    ####    ####    ####    ####
// Functions
// ####    ####    ####    ####    ####    ####    ####

// Helper function to get URL Parameter by name
function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  const regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  const results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Process email passed via query string
function processUrlParams() {

  const email = decodeURIComponent(getUrlParameter("email")) || undefined;
  const firstName = decodeURIComponent(getUrlParameter("first")) || undefined;
  const lastName = decodeURIComponent(getUrlParameter("last")) || undefined;

  if (email) {
    // Set the email field in the form
    const emailField = document.getElementById("email");
    emailField.setAttribute("value", email);

    // Add class to last next button to skip details and submit form
    const lastPane = $(
      "#pane-" + (quizData.questions.length - 1) + " button.next"
    );
    lastPane.addClass("submit-form").text("see results");

    // Remove Required from First and Last Name Fields
    $("#first-name").prop("required", false);
    $("#last-name").prop("required", false);

  }

  if(firstName){
    // Set the first name field in the form
    const firstNameField = document.getElementById("first-name");
    firstNameField.setAttribute("value", firstName);
  }

  if(lastName){
    // Set the last name field in the form
    const lastNameField = document.getElementById("last-name");
    lastNameField.setAttribute("value", lastName);
  }

}

// ####    ####    ####    ####    ####    ####    ####
// BUILD THE QUIZ DOM
// ####    ####    ####    ####    ####    ####    ####

// If there's hero data, build a hero <section>
if ("hero" in quizData) {
  // Add an image if available
  if ("image" in quizData.hero) {
    // Append some nested elements to the <body>
    $("body .quiz-content").append(
      $("<section />")
        .attr("id", "startingSection")
        .append(
          $("<div/>")
            .attr("class", "hero")
            .append(
              $("<img />").attr({
                src: quizData.hero.image.url,
                alt: quizData.hero.image.alt
              })
            )
        )
    );
  }
}

// Create the start button
const startDiv = $("<div />").attr({
  id: "startBoard",
  class: "board"
});

const startText = $("<p />")
  .text("We don't have a crystal ball, but we do have a fun sneak peek into your future college self!");

const startButton = $("<button />")
  .attr({
    id: "startTest",
    class: "startTest"
  })
  .text("Start");

startDiv.append(startText);
startDiv.append(startButton);
$("body .quiz-content").append(startDiv);

// Create the Form as var for later use
const formElem = $("<form />").attr({
  action: quizData.form.action,
  method: quizData.form.post,
  enctype: quizData.form.enctype,
  id: quizData.form.id
});

// Insert form into DOM
$("body .quiz-content").append(formElem);

// Add the Question Wrappers/Panes
const quizWrapper = $("<div />").attr({
  id: "quiz-wrapper"
});

// Loop over all questions and append to quizWrapper
$.each(quizData.questions, function(i, question) {
  // .quiz-pane wraps the heading and question in card-like pane displayed when active
  const quizPane = $("<div />").attr({
    id: "pane-" + i,
    class: "quiz-pane"
  });

  // Create the Title
  const questionTitle = $("<p />")
    .attr({
      class: "title"
    })
    .text(question.title);

  // Wrap the answer options
  const answerPane = $("<div />").attr({
    class:
      "question-pane question-" +
      i +
      " question-count-" +
      question.answers.length
  });

  // Loop over answers and create labels and inputs
  $.each(question.answers, function(j, answer) {
    const answerId = question.name.replace(/\s/g, "-") + "-" + j;

    const label = $("<label />")
      .attr({
        for: answerId
      })
      .text(answer.answer);

    const inputElem = $("<input />")
      .attr({
        id: answerId,
        class: "answer question-" + i,
        type: "radio",
        value: answer.answer,
        name: question.name
      })
      .attr("data-persona", answer.persona)
      .attr("data-question", "question-" + i);

    answerPane.append(inputElem);
    answerPane.append(label);
  });

  // Create Navigation Buttons
  const navWrapper = $("<div />").attr({
    class: "nav-wrapper"
  });

  // Previous Button
  const prevButton = $("<button />")
    .attr({
      class: "prev"
    })
    .attr("data-current-pane", "pane-" + i)
    .attr("data-future-pane", "pane-" + (i - 1))
    .text("Previous");

  // Next Button
  const nextButton = $("<button />")
    .attr({
      class: "next disabled question-" + i
    })
    .attr("data-current-pane", "pane-" + i)
    .attr("data-future-pane", "pane-" + (i + 1))
    .text("Next");

  // Don't show previous button when it isn't needed
  if (i > 0) {
    navWrapper.append(prevButton);
  }

  navWrapper.append(nextButton);

  quizPane
    .append(questionTitle)
    .append(answerPane)
    .append(navWrapper);

  quizWrapper.append(quizPane);
});

// Create a Pane for User Information
const userSlideID = quizData.questions.length;

const userPane = $("<div />").attr({
  id: "pane-" + userSlideID,
  class: "quiz-pane user-info"
});

// Loop Over Personal Fields and insert
$.each(quizData.userFields, function(k, field) {
  // Wrap field in Div
  const fieldWrapper = $("<div />").attr({
    class: "field-wrapper " + field.name.replace(/\s/g, "-").toLowerCase()
  });

  // If this is an input field
  if (field.type === "submit") {
    // Create an input with the vars
    const formElem = $("<input />").attr({
      id: "submit-btn",
      type: field.type,
      value: field.value
    });

    userPane.append(formElem);
  } else if (field.element === "input") {
    // Create label
    const formLabel = $("<label />")
      .attr({
        for: field.name.replace(/\s/g, "-").toLowerCase()
      })
      .text(field.name);

    if (field.type === "hidden") {
      formLabel.attr("hidden", "hidden");
    }

    // Create an input with the vars
    const formElem = $("<input />").attr({
      id: field.name.replace(/\s/g, "-").toLowerCase(),
      type: field.type,
      name: field.name
    });

    // Check for required fields
    if (field.hasOwnProperty("required")) {
      formElem.prop("required", true);
    }

    // Insert into DOM
    fieldWrapper.append(formLabel);
    fieldWrapper.append(formElem);
    userPane.append(fieldWrapper);
  } else if (field.element === "select") {
    // Create label
    const formLabel = $("<label />")
      .attr({
        for: field.name.replace(/\s/g, "-").toLowerCase()
      })
      .text(field.name);

    // Else if this is a select
    const formElem = $("<select />").attr({
      id: field.name.replace(/\s/g, "-").toLowerCase(),
      //class: 'answer question-' + i,
      type: field.element,
      name: field.name
    });

    // Loop over options and append
    $.each(field.options, function(i, option) {
      const selectOption = $("<option />")
        .attr({
          value: option.value
        })
        .text(option.value);

      formElem.append(selectOption);
    });

    // Insert into DOM
    fieldWrapper.append(formLabel);
    fieldWrapper.append(formElem);
    userPane.append(fieldWrapper);
  }
});

// Add navigation
// Create Navigation Buttons
const navWrapper = $("<div />").attr({
  class: "nav-wrapper"
});

// Get last question pane number
const lastQuestionPaneNum = quizData.questions.length;

// Previous Button
const prevButton = $("<button />")
  .attr({
    class: "prev"
  })
  .attr("data-current-pane", "pane-" + lastQuestionPaneNum)
  .attr("data-future-pane", "pane-" + (lastQuestionPaneNum - 1))
  .text("Previous");

// Submit Form
const submitInput = $("<input />").attr({
  class: "submit-form",
  id: "submit-btn",
  type: "submit",
  value: "See Results"
});

navWrapper.append(prevButton);
navWrapper.append(submitInput);
userPane.append(navWrapper);

quizWrapper.append(userPane);

// Insert Quiz into DOM
formElem.append(quizWrapper);

// Check if user email is set in query string
processUrlParams();

// ####    ####    ####    ####    ####    ####    ####
// Handle Interactions
// ####    ####    ####    ####    ####    ####    ####

// Events based on input changes
$("input.answer").change(function() {
  // Activate Next Button by getting Question ID and enabling it
  const questionNumber = $(this).data("question");
  $("button.next." + questionNumber).removeClass("disabled");

  // Set the label class to checked for this, and none for rest
  const questionId = $(this).data("question");

  // Get all children labels of div.questionID and clear the checked class
  const questionLabels = $("div." + questionId)
    .find("label")
    .removeClass("checked");

  const selectedLabelId = $(this).attr("id");

  $("label[for=" + selectedLabelId + "]").addClass("checked");

  // Initialize results counts
  let resultsCounts = [];
  $.each(quizData.results, function(i, answer) {
    resultsCounts.push({
      persona: answer,
      count: 0
    });
  });

  // Process Entries every change and set the field
  const answers = $(".question-pane");
  $.each(answers, function(i, answer) {
    const selectedAnswer = $(answer)
      .find("input[type=radio]:checked")
      .data("persona");

    // If a value is found, update the count
    if (selectedAnswer) {
      $.each(resultsCounts, function(i, answer) {
        if (answer.persona === selectedAnswer) {
          resultsCounts[i].count = resultsCounts[i].count + 1;
        }
      });
    }
  });

  // Sort the Results
  resultsCounts.sort(function(a, b) {
    return a.count > b.count ? -1 : 1;
  });

  // Default the Persona
  let persona = quizData.defaultResult;

  // If there is not a tie, set to correct persona
  if (resultsCounts[0].count != resultsCounts[1].count) {
    persona = resultsCounts[0].persona;
  }

  // Set the Persona Input Field
  $("input#persona").val(persona);
});

// Start the Quiz
$("button#startTest").click(function(event) {
  event.preventDefault();
  $("#startBoard").hide();
  $("#pane-0").toggleClass("active");
});

// Navigate through the quiz
$(".nav-wrapper button").click(function(event) {
  event.preventDefault();

  // Check for submit-form class, and submit form when found
  if ($(this).hasClass("submit-form")) {
    $("#quiz-form").submit();
    return;
  }

  // Hide current pane
  const currentPane = $(this).data("current-pane");
  $("#" + currentPane).toggleClass("active");

  // Show future pane
  const futurePane = $(this).data("future-pane");
  $("#" + futurePane).toggleClass("active");
});
